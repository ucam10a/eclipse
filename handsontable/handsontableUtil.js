/**
 * handsontable utility
 * 
 */
var HandsontableUtil = $Class.extend({
    parentWin : null,
    hot : null,
    container : null,
    init: function(parentWin, hot, container){
        this.parentWin = parentWin;
        this.hot = hot;
        this.container = container;
        if (hot != null) {
            this.setup();
        }
        return this;
    },
    resizeFrame : function() {
        if (typeof this.parentWin['resizeHandsontableFrame'] == 'undefined') {
            return;
        }
        var top = $("div[class='ht_clone_top handsontable']");
        if (top == null) {
            this.resizeFrame();
        } else {
            var tableWidth = top.width();
            this.parentWin['resizeHandsontableFrame'](tableWidth);
        }
    },
    createEmptyData : function (rowNum, colNum) {
        var data = [];
        for (var i = 0; i < rowNum; i++) {
            var r = [];
            for (var j = 0; j < colNum; j++) {
                r.push("");
            }
            data.push(r);
        }
        return data;
    },
    setup : function () {
        var settings = this.hot.getSettings();
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        var util = this;
        updateSettings["cells"] = function (row, col, prop) {
            this.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                Handsontable.renderers.TextRenderer.apply(this, arguments);
                HandsontableUtil.test.apply(util, arguments);
            };
        };
        var columns = settings["columns"];
        if (columns == null) {
            columns = [];
            var colCnt = this.hot.countCols();
            for (var i = 0; i < colCnt; i++) {
                var header = this.hot.getColHeader(i);
                var col = {};
                col["data"] = header;
                col["editor"] = "text";
                col["type"] = "text";
                columns.push(col);
            }
        }
        updateSettings["columns"] = columns;
        var colWidths = settings["colWidths"];
        if (colWidths == null) {
            colWidths = [];
            var colCnt = this.hot.countCols();
            for (var i = 0; i < colCnt; i++) {
                colWidths.push(60);
            }
        }
        updateSettings["colWidths"] = colWidths;
        this.hot.updateSettings(updateSettings);
    },
    setData : function(data) {
        if (data !=  null) {
            if (data.length > 0 && $.isArray(data[0])) {
                this.setArrayData(data);
            } else {
                this.setObjectData(data);
            }
        }
    },
    setObjectData : function (objSource) {
        this.hot.loadData(objSource);
        this.hot.render();
    },
    setArrayData : function (dataArray) {
        var objSource = this.convertToObjectSource(dataArray);
        this.hot.loadData(objSource);
        this.hot.render();
    },
    convertToObjectSource : function (dataObj) {
        var objSource = [];
        var colCnt = dataObj[0].length;
        for (var i = 0; i < dataObj.length; i++) {
            var col = {};
            for (var j = 0; j < colCnt; j++) {
                var header = this.hot.getColHeader(j);
                col[j + ""] = dataObj[i][j];
            }
            objSource.push(col);
        }
        return objSource;
    },
    setCellValue : function (rowIdx, colIdx, value) {
        this.hot.setDataAtCell(rowIdx, colIdx, value);
    },
    getCellValue : function (rowIdx, colIdx) {
        return this.hot.getDataAtCell(rowIdx, colIdx);
    },
    getColumnNum : function () {
        return this.hot.countCols();
    },
    getRowNum : function () {
        return this.hot.countRows();
    },
    setColumnEdit : function (colIdx, edit, withColor) {
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        if (edit == false) {
            columns[colIdx]["editor"] = false;
            if (withColor === true) columns[colIdx]["renderer"] = lockRenderer;
        } else if (edit == 'numeric') {
            columns[colIdx]["editor"] = "numeric";
            columns[colIdx]["renderer"] = "numeric";
        } else if (edit == 'text') {
            columns[colIdx]["editor"] = "text";
            columns[colIdx]["renderer"] = "text";
        } else {
            alert("unknown edit: " + edit);
            return;
        }
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        updateSettings["columns"] = columns;
        this.hot.updateSettings(updateSettings);
    },
    lockAll : function() {
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        for (var i = 0; i < columns.length; i++) {
            columns[i]["editor"] = false;
            columns[i]["renderer"] = "text";
        }
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        updateSettings["columns"] = columns;
        this.hot.updateSettings(updateSettings);
    },
    unlockAll : function() {
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        for (var i = 0; i < columns.length; i++) {
            columns[i]["editor"] = "text";
            columns[i]["renderer"] = "text";
        }
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        updateSettings["columns"] = columns;
        this.hot.updateSettings(updateSettings);
    },
    setColumnType : function (colIdx, type) {
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        if (type == "text") {
            columns[colIdx]["type"] = "text";
        } else if (type == 'date') {
            columns[colIdx]["type"] = "date";
        } else if (type == 'checkbox') {
            columns[colIdx]["type"] = "checkbox";
        } else if (type == 'numeric') {
            columns[colIdx]["type"] = "numeric";
        } else {
            alert("unknown type: " + type);
            return;
        }
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        updateSettings["columns"] = columns;
        this.hot.updateSettings(updateSettings);
    },
    setColumnRender : function (colIdx, renderer) {
        var settings = this.hot.getSettings();
        var columns = settings["columns"];
        if (renderer == "text") {
            columns[colIdx]["renderer"] = "text";
        } else if (renderer == 'html') {
            columns[colIdx]["renderer"] = "html";
        } else {
            alert("unknown renderer: " + renderer);
            return;
        }
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        updateSettings["columns"] = columns;
        this.hot.updateSettings(updateSettings);
    },
    setColumnWidth : function (colIdx, width) {
        var settings = this.hot.getSettings();
        var colWidths = settings["colWidths"];
        var updateSettings = {};
        for (var key in settings) {
            var val = settings[key];
            if (val != null) {
                updateSettings[key] = val;
            }
        }
        colWidths[colIdx] = width;
        updateSettings["colWidths"] = colWidths;
        this.hot.updateSettings(updateSettings);
    },
    hideColumn : function (colIdx) {
        this.setColumnWidth(colIdx, 0.1);
    },
    hideRow : function (rowIdx) {
        var tr1 = $('table.htCore:eq(2) tr:eq(' + rowIdx + ')');
        var tr2 = $('table.htCore:eq(0) tr:eq(' + rowIdx + ')');
        tr1.css("display", "none");
        tr2.css("display", "none");
    },
    showRow : function (rowIdx) {
        var tr1 = $('table.htCore:eq(2) tr:eq(' + rowIdx + ')');
        var tr2 = $('table.htCore:eq(0) tr:eq(' + rowIdx + ')');
        tr1.css("display", "");
        tr2.css("display", "");
    },
    getSelected : function () {
        return this.hot.getSelected();
    }
});
HandsontableUtil.test = function (instance, td, row, col, prop, value, cellProperties) {
    var a = this;
}